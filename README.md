# Git-Teleport Website

This repository contains the Gitlab CI/CD glue for the git-teleport website,

<https://git-teleport.gitlab.io/>

The contents of the website itself may be found in our main repository,

```bash
git clone https://gitlab.com/git-teleport/git-teleport.git
```

Then see the documentation directory [`share/git-teleport/doc`](https://gitlab.com/git-teleport/git-teleport/-/tree/develop/share/git-teleport/doc).

All issues with git-teleport, including the website, should be directed to our main [issue tracker](https://gitlab.com/git-teleport/git-teleport/-/issues).
