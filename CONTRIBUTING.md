# CONTRIBUTING

The `git-teleport` project welcomes contributions from all users, regardless of experience or affiliation. Before making a contribution, please read and adhere to the instructions at <https://gitlab.com/git-teleport/git-teleport/-/blob/develop/CONTRIBUTING.md>.

This repository is for Gitlab Pages CI/CD build rules only. All issues with git-teleport, including the website, should be directed to our main [issue tracker](https://gitlab.com/git-teleport/git-teleport/-/issues).
